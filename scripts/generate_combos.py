from __future__ import print_function
from __future__ import absolute_import
import sys
import six
import io
import time
from subprocess import Popen
try:
    import WtFit.templates as templates
    import WtFit.shortcuts as shortcuts
except ImportError:
    sys.path.append('..')
    import WtFit.templates as templates
    import WtFit.shortcuts as shortcuts
try:
    from pathlib import PosixPath
except ImportError:
    from pathlib2 import PosixPath
import logging
logging.basicConfig(level=logging.INFO,
                    format='[%(levelname)s]\t%(name)s\t%(message)s')
logger = logging.getLogger('fit_matrix')


sr_options = [4, 5, 6, 7]
sr_combos = []
for z1j1b in sr_options:
    for z2j1b in sr_options:
        for z2j2b in sr_options:
            sr_combos.append((str(z1j1b), str(z2j1b), str(z2j2b)))

r2j2bsel_options = [
    "reg2j2bLmm==1&&elmu==1&&OS==1",
    "reg2j2b==1&&elmu==1&&OS==1"
]

extra_CR_options = [
    None,
    "njets==4&&elmu==1&&OS==1",
    "njets==3&&elmu==1&&OS==1"
]


class fit_test(object):
    def __init__(self, z_set, r2j2bsel, CRsel):
        self.z1j1b = z_set[0]
        self.z2j1b = z_set[1]
        self.z2j2b = z_set[2]
        self.r2j2bsel = r2j2bsel
        self.CRsel = CRsel

    @property
    def outdir(self):
        if self.CRsel is None:
            nj = 'NOCR'
        elif 'njets==3' in self.CRsel:
            nj = '3j'
        else:
            nj = '4j'
        return 'fit_{z1j1b}{z2j1b}{z2j2b}_{r2j2b}_{nj}'.format(
            z1j1b=self.z1j1b,
            z2j1b=self.z2j1b,
            z2j2b=self.z2j2b,
            r2j2b='CUT' if 'Lmm' in self.r2j2bsel else 'NOCUT',
            nj=nj
        )


def generate_rundirs():
    dirs = []
    for sr_combo in sr_combos:
        for r2j2bsel in r2j2bsel_options:
            for extra_CR in extra_CR_options:
                ft = fit_test(sr_combo, r2j2bsel, extra_CR)
                outdir = PosixPath(ft.outdir)
                outdir.mkdir(exist_ok=False)

                Fit1 = templates.Fit(Name="WtFit_res",
                                     NumCPU="2",
                                     FitBlind="TRUE")

                Job1 = templates.Job(
                    Name="WtFit",
                    NtuplePaths="~/bdt_fitting/main_bdt",
                    DebugLevel="1"
                )

                SR1 = templates.Region_SIGNAL_TransfoD(
                    Name="reg1j1b",
                    Selection="reg1j1b==1&&elmu==1&&OS==1",
                    Variable="bdt_response",
                    VariableTitle="BDT",
                    nbins="40",
                    xmin="-0.8",
                    xmax="0.8",
                    Label="1j1b",
                    ShortLabel="1j1b",
                    zs=float(ft.z1j1b),
                    zb=float(ft.z1j1b),
                )

                SR2 = templates.Region_SIGNAL_TransfoD(
                    Name="reg2j1b",
                    Selection="reg2j1b==1&&elmu==1&&OS==1",
                    Variable="bdt_response",
                    VariableTitle="BDT",
                    nbins="40",
                    xmin="-0.8",
                    xmax="0.8",
                    Label="2j1b",
                    ShortLabel="2j1b",
                    zs=float(ft.z2j1b),
                    zb=float(ft.z2j1b),
                )

                SR3 = templates.Region_SIGNAL_TransfoD(
                    Name="reg2j2b",
                    Selection=ft.r2j2bsel,
                    Variable="bdt_response",
                    VariableTitle="BDT",
                    nbins="40",
                    xmin="-0.4",
                    xmax="0.6",
                    Label="2j2b",
                    ShortLabel="2j2b",
                    zs=float(ft.z2j2b),
                    zb=float(ft.z2j2b),
                )

                if ft.CRsel is not None:
                    CR1 = templates.Region_CONTROL(
                        Name="reg3j" if "njets==3" in ft.CRsel else "reg4j",
                        Selection=ft.CRsel,
                        Variable="pT_jet2",
                        VariableTitle="#it{p}_{T}^{jet2}",
                        nbins="20",
                        xmin="25",
                        xmax="225",
                        Label="3j" if "njets==3" in ft.CRsel else "4j",
                        ShortLabel="3j" if "njets==3" in ft.CRsel else "4j",
                    )
                else:
                    CR1 = ''

                config_file_content = ''

                top_blocks = [Job1, Fit1]
                region_blocks = [SR1, SR2, SR3, CR1]
                for tb in top_blocks:
                    config_file_content += tb
                    config_file_content += '\n'
                for rb in region_blocks:
                    config_file_content += rb
                    config_file_content += '\n'

                config_file_content += shortcuts.sample_blocks
                config_file_content += '\n'
                config_file_content += shortcuts.norm_factor_blocks
                config_file_content += '\n'
                config_file_content += shortcuts.modelling_systematics_blocks
                config_file_content += '\n'
                config_file_content += shortcuts.weight_systematics_blocks
                config_file_content += '\n'
                config_file_content += shortcuts.tree_systematics_2s
                config_file_content += '\n'
                config_file_content += shortcuts.tree_systematics_1s
                config_file_content += '\n'
                if six.PY2:
                    config_file_content = unicode(config_file_content)
                with io.open(str(outdir / 'fit.config'), 'w',
                             encoding="utf-8") as outfile:
                    outfile.write(config_file_content)
                logger.info('{} created '.format(outdir))
                dirs.append(str(outdir))

    return dirs


def run_commands(dirs):
    if not dirs:
        return
    total_done = 0
    total_jobs = len(dirs)

    def done(p):
        return p.poll() is not None

    def success(p):
        return p.returncode == 0

    def fail():
        sys.exit(1)

    max_task = 8
    processes = []
    logger.info('Starting parallel processes')
    while True:
        while dirs and len(processes) < max_task:
            rundir = dirs.pop()
            stdoutname = '{}/stdout.log'.format(rundir)
            stderrname = '{}/stdout.err'.format(rundir)
            with open(stdoutname, 'w') as sout, open(stderrname, 'w') as serr:
                command = '# trex-fitter nwdf fit.config'
                processes.append(Popen(command,
                                       shell=True, cwd=rundir,
                                       stdout=sout, stderr=serr))

        for p in processes:
            if done(p):
                if success(p):
                    total_done += 1
                    logger.info('{} is done ({}/{})'.format(
                        p, total_done, total_jobs))
                    processes.remove(p)
                else:
                    logger.error(
                        'Failure return code, check log for {}'.format(p))
                    fail()

        if not processes and not dirs:
            break
        else:
            time.sleep(0.05)


if __name__ == '__main__':
    dirs = generate_rundirs()
    run_commands(dirs)
