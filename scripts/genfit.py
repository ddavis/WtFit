import sys
import WtFit.templates as templates
import WtFit.shortcuts as shortcuts

#Job1 = templates.Job(
#    Name="test1", NtuplePaths="/Users/ddavis/ATLAS/datasets/nominal_bdt", DebugLevel="1"
#)

Job1 = templates.Job(
     Name="test1",
     NtuplePaths="/var/phy/project/hep/atlas/users/drd25/top/analysis/run/wtn_20181116/main_bdt",
     DebugLevel="1"
)

Fit1 = templates.Fit(Name="test1fit", NumCPU="8", FitBlind="TRUE")

SR1 = templates.Region_SignalTransfoD(
    Name="reg1j1b",
    Selection="reg1j1b==1&&elmu==1&&OS==1",
    Variable="bdt_response",
    VariableTitle="BDT",
    nbins="40",
    xmin="-0.8",
    xmax="0.8",
    Label="1j1b",
    ShortLabel="1j1b",
    zs="8.0",
    zb="8.0",
)

SR2 = templates.Region_SignalTransfoD(
    Name="reg2j1b",
    Selection="reg2j1b==1&&elmu==1&&OS==1",
    Variable="bdt_response",
    VariableTitle="BDT",
    nbins="40",
    xmin="-0.8",
    xmax="0.8",
    Label="2j1b",
    ShortLabel="2j1b",
    zs="8.0",
    zb="8.0",
)

SR3 = templates.Region_SignalTransfoD(
    Name="reg2j2b",
    Selection="reg2j2b==1&&elmu==1&&OS==1",
    Variable="bdt_response",
    VariableTitle="BDT",
    nbins="40",
    xmin="-0.4",
    xmax="0.6",
    Label="2j2b",
    ShortLabel="2j2b",
    zs="8.0",
    zb="8.0",
)

CR1 = templates.Region_Control(
    Name="reg3j",
    Selection="njets==3&&elmu==1&&OS==1",
    Variable="pT_jet2",
    VariableTitle="#it{p}_{T}^{jet2}",
    nbins="20",
    xmin="25",
    xmax="225",
    Label="3j",
    ShortLabel="3j",
)

top_blocks = [Job1, Fit1]

region_blocks = [SR1, SR2, SR3, CR1]


for tb in top_blocks:
    print(tb)
for rb in region_blocks:
    print(rb)

print(shortcuts.sample_blocks)
print(shortcuts.norm_factor_blocks)
print(shortcuts.modelling_systematics_blocks)
print(shortcuts.weight_systematics_blocks)
print(shortcuts.tree_systematics_2s)
print(shortcuts.tree_systematics_1s)
