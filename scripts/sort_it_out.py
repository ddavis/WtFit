try:
    from pathlib import PosixPath
except ImportError:
    from pathlib2 import PosixPath

class fit:
    def __init__(self, name, val):
        self.name = name
        self.val = val
    def __repr__(self):
        return '{0:1.5f} {1}'.format(self.val, self.name)

fits = []
wd = PosixPath('.')
for path in wd.iterdir():
    if 'fit_' not in str(path):
        continue
    fit_output = path / 'WtFit/Fits/WtFit.txt'
    if fit_output.exists():
        vals = fit_output.read_text().splitlines()[1].split()[2:]
        p = abs(float(vals[0]))
        m = abs(float(vals[1]))
        a = (p+m)/2
        f = fit(str(path), a)
        fits.append(f)
fits.sort(key=lambda x: x.val, reverse=True)

#for f in fits[-10:]:
for i, f in enumerate(fits):
    print('{0:>3}  {1}'.format(i, f))
