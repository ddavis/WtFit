from __future__ import absolute_import
import WtFit.templates as templates
import WtFit.constants as constants

tWghost = templates.Sample_GHOST(
    Name="tWghost",
    NtupleFiles=(
        "tW_DR_410648_AFII_MC16a_nominal.bdt_response,"
        "tW_DR_410648_AFII_MC16d_nominal.bdt_response,"
        "tW_DR_410649_AFII_MC16a_nominal.bdt_response,"
        "tW_DR_410649_AFII_MC16d_nominal.bdt_response"
    ),
)

ttbarghost = templates.Sample_GHOST(
    Name="ttbarghost",
    NtupleFiles=(
        "ttbar_410472_AFII_MC16a_nominal.bdt_response,"
        "ttbar_410472_AFII_MC16d_nominal.bdt_response"
    ),
)

Data = templates.Sample_DATA(
    NtupleFiles=(
        "Data15_data15_Data_Data_nominal.bdt_response,"
        "Data16_data16_Data_Data_nominal.bdt_response,"
        "Data17_data17_Data_Data_nominal.bdt_response"
    )
)

tW = templates.Sample(
    Name="tW",
    Title="#it{tW}",
    TexTitle="$tW$",
    FillColor=861,
    LineColor=1,
    Type="SIGNAL",
    NtupleFiles=(
        "tW_DR_410648_FS_MC16a.bdt_response,"
        "tW_DR_410648_FS_MC16d.bdt_response,"
        "tW_DR_410649_FS_MC16a.bdt_response,"
        "tW_DR_410649_FS_MC16d.bdt_response"
    ),
)

ttbar = templates.Sample(
    Name="ttbar",
    Title="#it{t#bar{t}}",
    TexTitle="$t\bar{t}$",
    FillColor=633,
    LineColor=1,
    Type="BACKGROUND",
    NtupleFiles=(
        "ttbar_410472_FS_MC16a.bdt_response,"
        "ttbar_410472_FS_MC16d.bdt_response"
    ),
)

Zjets = templates.Sample(
    Name="Zjets",
    Title="#it{Z}+jets",
    TexTitle="$Z$+jets",
    FillColor=801,
    LineColor=1,
    Type="BACKGROUND",
    NtupleFiles=("Zjets_all"),
)

Diboson = templates.Sample(
    Name="Diboson",
    Title="Diboson",
    TexTitle="Doboson",
    FillColor=400,
    LineColor=1,
    Type="BACKGROUND",
    NtupleFiles=("Diboson_all"),
)

MCNP = templates.Sample(
    Name="MCNP",
    Title="MCNP",
    TexTitle="Doboson",
    FillColor="615",
    LineColor="1",
    Type="BACKGROUND",
    NtupleFiles=("MCNP_all"),
)

NF1 = templates.NormFactor(
    Name="SigXsecOverSM",
    Title="#it{#mu}_{#it{tW}}",
    Nominal="1",
    Min="0",
    Max="2",
    Samples="tW",
)

NF2 = templates.NormFactor(
    Name="mu_ttbar",
    Title="#it{#mu}_{#it{t#bar{t}}}",
    Nominal="1",
    Min="0",
    Max="2",
    Samples="ttbar",
)

sys_Lumi = templates.Systematic_OVERALL(
    Name="Lumi",
    Title="Lumi",
    OverallUp=0.023,
    OverallDown=-0.023,
    Samples="all",
    Category="Instrumental",
)

sys_Disboson_Norm = templates.Systematic_OVERALL(
    Name="Norm_Diboson",
    Title="Norm Diboson",
    OverallUp="0.5",
    OverallDown="-0.5",
    Samples="Diboson",
    Category="Norms",
)

sys_Zjets_Norm = templates.Systematic_OVERALL(
    Name="Norm_Zjets",
    Title="Norm Z+jets",
    OverallUp="0.5",
    OverallDown="-0.5",
    Samples="Zjets",
    Category="Norms",
)

sys_tW_DRDS = templates.Systematic_tW_DRDS(
    NtupleFilesUp=(
        "tW_DS_410656_FS_MC16a_nominal.bdt_response,"
        "tW_DS_410657_FS_MC16a_nominal.bdt_response,"
        "tW_DS_410656_FS_MC16d_nominal.bdt_response,"
        "tW_DS_410657_FS_MC16d_nominal.bdt_response"
    ),
    Smoothing="40",
)

sys_tW_HS = templates.Systematic_HISTO_ReqRefSamp(
    Name="tW_HS",
    Title="tW HS",
    Sample="tW",
    ReferenceSample="tWghost",
    NtupleFilesUp=(
        "tW_412003_AFII_MC16a_nominal.bdt_response,"
        "tW_412003_AFII_MC16d_nominal.bdt_response"
    ),
    Smoothing="40",
)

sys_tW_PS = templates.Systematic_HISTO_ReqRefSamp(
    Name="tW_PS",
    Title="tW PS",
    Sample="tW",
    ReferenceSample="tWghost",
    NtupleFilesUp=(
        "tW_DR_411038_AFII_MC16a_nominal.bdt_response,"
        "tW_DR_411038_AFII_MC16d_nominal.bdt_response,"
        "tW_DR_411039_AFII_MC16a_nominal.bdt_response,"
        "tW_DR_411039_AFII_MC16d_nominal.bdt_response"
    ),
    Smoothing="40",
)

sys_tW_addrad = templates.Systematic_tW_AR(
    Smoothing="40"
)

sys_ttbar_HS = templates.Systematic_HISTO_ReqRefSamp(
    Name="ttbar_HS",
    Title="t#bar{t} HS",
    Sample="ttbar",
    ReferenceSample="ttbarghost",
    NtupleFilesUp=(
        "ttbar_410465_AFII_MC16a_nominal.bdt_response,"
        "ttbar_410465_AFII_MC16d_nominal.bdt_response"
    ),
    Smoothing="40",
)

sys_ttbar_PS = templates.Systematic_HISTO_ReqRefSamp(
    Name="ttbar_PS",
    Title="t#bar{t} PS",
    Sample="ttbar",
    ReferenceSample="ttbarghost",
    NtupleFilesUp=(
        "ttbar_410558_AFII_MC16a_nominal.bdt_response,"
        "ttbar_410558_AFII_MC16d_nominal.bdt_response"
    ),
    Smoothing="40",
)

sys_ttbar_addrad = templates.Systematic_ttbar_AR(
    Smoothing="40",
    NtupleFilesUp=(
        "ttbar_410482_AFII_MC16a_nominal.bdt_response,"
        "ttbar_410482_AFII_MC16d_nominal.bdt_response"
    ),
    NtupleFilesDown=(
        "ttbar_410472_AFII_MC16a_nominal.bdt_response,"
        "ttbar_410472_AFII_MC16d_nominal.bdt_response"
    ),
)

sys_weights = []
for title, options in constants.SYS_WEIGHTS.items():
    sys_weights.append(
        templates.Systematic_HISTO_Weight2s(
            Name=title,
            Title=title,
            WeightUp=options[0],
            WeightDown=options[1],
            Smoothing=options[3],
            Category=options[2],
        )
    )

sys_trees_2s = []
sys_trees_1s = []
for title, options in constants.SYS_TREES_TWOSIDED.items():
    sys_trees_2s.append(
        templates.Systematic_HISTO_Tree2s(
            Name=title,
            Title=title,
            NtupleNameUp='WtTMVA_{}'.format(options[0]),
            NtupleNameDown='WtTMVA_{}'.format(options[1]),
            Smoothing=options[3],
            Category=options[2],
        )
    )

for title, options in constants.SYS_TREES_ONESIDED.items():
    sys_trees_1s.append(
        templates.Systematic_HISTO_Tree1s(
            Name=title,
            Title=title,
            NtupleNameUp='WtTMVA_{}'.format(options[0]),
            Smoothing=options[2],
            Category=options[1],
        )
    )

sample_blocks = "\n".join([tWghost, ttbarghost, Data,
                           tW, ttbar, Zjets, Diboson, MCNP])
norm_factor_blocks = "\n".join([NF1, NF2, sys_Lumi,
                                sys_Disboson_Norm, sys_Zjets_Norm])
modelling_systematics_blocks = "\n".join(
    [sys_tW_DRDS, sys_tW_PS, sys_tW_HS, sys_tW_addrad,
     sys_ttbar_PS, sys_ttbar_HS, sys_ttbar_addrad]
)

weight_systematics_blocks = "\n".join(sys_weights)
tree_systematics_2s = "\n".join(sys_trees_2s)
tree_systematics_1s = "\n".join(sys_trees_1s)
