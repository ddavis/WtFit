"""
Templates for creating TRExFitter blocks. Heavily tailored for
downstream tW datasets
"""


def Job(Name="WtFit", NtuplePaths=".", DebugLevel="1"):
    """
    Arguments
    ---------
    Name: str
    NtuplePaths: str
    DebugLevel: str
    """
    _job = """
Job: "{Name}"
  CmeLabel: "13 TeV"
  POI: "SigXsecOverSM"
  ReadFrom: NTUP
  NtuplePaths: "{NtuplePaths}"
  Label: "#it{{e^{{#pm}}#mu^{{#mp}}}}"
  LumiLabel: "80.5 fb^{{-1}}"
  MCweight: "weight_nominal"
  Lumi: 80.5
  PlotOptions: YIELDS,NOXERR
  NtupleName: "WtTMVA_nominal"
  DebugLevel: {DebugLevel}
  MCstatThreshold: 0.001
  SystControlPlots: TRUE
  SystPruningShape: 0.005
  SystPruningNorm: 0.005
  ImageFormat: "pdf"
  SystCategoryTables: TRUE
  RankingPlot: all
  RankingMaxNP: 15
  DoSummaryPlot: TRUE
  DoTables: TRUE
  DoSignalRegionsPlot: TRUE
  DoPieChartPlot: TRUE"""
    block = _job.format(Name=Name,
                        NtuplePaths=NtuplePaths,
                        DebugLevel=DebugLevel)
    return block


def Fit(Name="WtFit", NumCPU="4", FitBlind="TRUE"):
    """
    Arguments
    ---------
    Name : str
    NumCPU : str
    FitBlind : str
      TRUE or FALSE
    """
    _fit = """
Fit: "{Name}"
  FitType: SPLUSB
  FitRegion: CRSR
  NumCPU: {NumCPU}
  POIAsimov: 1
  FitBlind: {FitBlind}"""
    block = _fit.format(Name=Name, NumCPU=NumCPU, FitBlind=FitBlind)
    return block


def Region_SIGNAL_TransfoD(Name="", Selection="",
                           VariableTitle="", Variable="",
                           nbins="20", xmin="0", xmax="20",
                           Label="", ShortLabel="",
                           zs="5.0", zb="5.0"):
    _signal_region = '''
Region: "{Name}"
  Type: SIGNAL
  Selection: "{Selection}"
  VariableTitle: "{VariableTitle}"
  Variable: "{Variable}",{nbins},{xmin},{xmax}
  Label: "{Label}"
  ShortLabel: "{ShortLabel}"'''
    _signal_region_transfoD = """
{base}
  Binning: "AutoBin","TransfoD",{zs},{zb}"""
    block = _signal_region_transfoD.format(
        base=_signal_region.format(
            Name=Name,
            Selection=Selection,
            VariableTitle=VariableTitle,
            Variable=Variable,
            nbins=nbins,
            xmin=xmin,
            xmax=xmax,
            Label=Label,
            ShortLabel=ShortLabel,
        ),
        zs=zs,
        zb=zb,
    )
    return block


def Region_CONTROL(Name="", Selection="",
                   VariableTitle="", Variable="",
                   nbins="", xmin="", xmax="",
                   Label="", ShortLabel=""):
    """
    Arguments
    ---------
    Name : str
    Selection : str
    VariableTitle  : str
    Variable : str
    nbins : int
    xmin : float
    xmax : float
    Label : str
    ShortLabel: str
    """
    _control_region = '''
Region: "{Name}"
  Type: CONTROL
  Selection: "{Selection}"
  VariableTitle: "{VariableTitle}"
  Variable: "{Variable}",{nbins},{xmin},{xmax}
  Label: "{Label}"
  ShortLabel: "{ShortLabel}"'''
    block = _control_region.format(
        Name=Name,
        Selection=Selection,
        VariableTitle=VariableTitle,
        Variable=Variable,
        nbins=nbins,
        xmin=xmin,
        xmax=xmax,
        Label=Label,
        ShortLabel=ShortLabel,
    )
    return block


def Sample_GHOST(Name="", NtupleFiles=""):
    """
    Name : str
    NtupleFiles : str
    """
    _ghost = """
Sample: "{Name}"
  Type: GHOST
  NtupleFiles: {NtupleFiles}"""
    block = _ghost.format(Name=Name, NtupleFiles=NtupleFiles)
    return block


def Sample_DATA(NtupleFiles=""):
    """
    NtupleFiles : str
    """
    _data_sample = """
Sample: "Data"
  Type: DATA
  Title: "Data"
  NtupleFiles: {NtupleFiles}"""
    block = _data_sample.format(NtupleFiles=NtupleFiles)
    return block


def Sample(Name="", Type="",
           Title="", TexTitle="",
           FillColor="", LineColor="",
           NtupleFiles=""):
    """
    Name : str
    Type : str
    Title : str
    TexTitle : str
    FillColor : int
    LineColor : int
    NtupleFiles : str
    """
    _sample = """
Sample: "{Name}"
  Type: {Type}
  Title: "{Title}"
  TexTitle: "{TexTitle}"
  FillColor: {FillColor}
  LineColor: {LineColor}
  NtupleFiles: {NtupleFiles}"""
    block = _sample.format(
        Name=Name,
        Type=Type,
        Title=Title,
        TexTitle=TexTitle,
        FillColor=FillColor,
        LineColor=LineColor,
        NtupleFiles=NtupleFiles,
    )
    return block


def NormFactor(Name="", Title="", Nominal="", Min="", Max="", Samples=""):
    """
    Name : str
    Title : str
    Nominal : float
    Min : float
    Max : float
    Samples : str
    """
    _norm_factor = """
NormFactor: "{Name}"
  Title: "{Title}"
  Nominal: {Nominal}
  Min: {Min}
  Max: {Max}
  Samples: {Samples}"""
    block = _norm_factor.format(
        Name=Name,
        Title=Title,
        Nominal=Nominal,
        Min=Min,
        Max=Max,
        Samples=Samples
    )
    return block


def Systematic_HISTO_Weight2s(Name="", Title="",
                              WeightUp="", WeightDown="",
                              Smoothing="", Category=""):
    """
    Name : str
    Title : str
    WeightUp : str
    WeightDown : str
    Smoothing : int
    Category : str
    """
    _weight_systematic = '''
Systematic: "{Name}"
  Type: HISTO
  Title: "{Title}"
  Samples: tW, ttbar
  WeightUp: (1.0/weight_nominal)*{WeightUp}
  WeightDown: (1.0/weight_nominal)*{WeightDown}
  Smoothing: {Smoothing}
  Symmetrisation: TWOSIDED
  Category: "{Category}"'''
    block = _weight_systematic.format(
        Name=Name,
        Title=Title,
        WeightUp=WeightUp,
        WeightDown=WeightDown,
        Smoothing=Smoothing,
        Category=Category,
    )
    return block


def Systematic_HISTO_Tree2s(Name="", Title="", NtupleNameUp="",
                            NtupleNameDown="", Smoothing="", Category=""):
    """
    Name : str
    Title : str
    NtupleNameUp : str
    NtupleNameDown: str
    Smoothing : str
    Category : str
    """
    _tree_systematic = '''
Systematic: "{Name}"
  Title: "{Title}"
  Type: HISTO
  Samples: tW, ttbar
  NtupleNameUp: {NtupleNameUp}
  NtupleNameDown: {NtupleNameDown}
  Smoothing: {Smoothing}
  Symmetrisation: TWOSIDED
  Category: "{Category}"'''
    block = _tree_systematic.format(
        Name=Name,
        Title=Title,
        NtupleNameUp=NtupleNameUp,
        NtupleNameDown=NtupleNameDown,
        Smoothing=Smoothing,
        Category=Category,
    )
    return block


def Systematic_HISTO_Tree1s(Name="", Title="", NtupleNameUp="",
                            Smoothing="", Category=""):
    """
    Name : str
    Title : str
    NtupleNameUp : str
    NtupleNameDown: str
    Smoothing : str
    Category : str
    """
    _tree_systematic = '''
Systematic: "{Name}"
  Title: "{Title}"
  Type: HISTO
  Samples: tW, ttbar
  NtupleNameUp: {NtupleNameUp}
  Smoothing: {Smoothing}
  Symmetrisation: ONESIDED
  Category: "{Category}"'''
    block = _tree_systematic.format(
        Name=Name,
        Title=Title,
        NtupleNameUp=NtupleNameUp,
        Smoothing=Smoothing,
        Category=Category,
    )
    return block


def Systematic_OVERALL(Name="", Title="", OverallUp="", OverallDown="",
                       Samples="", Category=""):
    """
    Name : str
    Title : str
    OverallUp : str
    OverallDown : str
    Samples : str
    Category : str
    """
    _overall_systematic = '''
Systematic: "{Name}"
  Type: OVERALL
  Title: "{Title}"
  OverallUp: {OverallUp}
  OverallDown: {OverallDown}
  Samples: {Samples}
  Category: "{Category}"'''
    block = _overall_systematic.format(
        Name=Name,
        Title=Title,
        OverallUp=OverallUp,
        OverallDown=OverallDown,
        Samples=Samples,
        Category=Category,
    )
    return block


def Systematic_tW_AR(Smoothing=""):
    _tw_addrad = '''
Systematic: tW_AR
  Title: "tW Additional Rad."
  Type: HISTO
  Samples: tW
  Smoothing: {Smoothing}
  Symmetrisation: TWOSIDED
  WeightDown: weight_sys_radLo*(1.0/weight_nominal)
  WeightUp: weight_sys_radHi*(1.0/weight_nominal)
  Category: "Modelling"'''
    block = _tw_addrad.format(Smoothing=Smoothing)
    return block


def Systematic_ttbar_AR(Smoothing="", NtupleFilesUp="", NtupleFilesDown=""):
    _ttbar_addrad = '''
Systematic: ttbar_AR
  Title: "t#bar{{t}} Additional Rad."
  Type: HISTO
  Samples: ttbar
  Smoothing: {Smoothing}
  ReferenceSample: ttbarghost
  Symmetrisation: TWOSIDED
  NtupleFilesUp: {NtupleFilesUp}
  NtupleFilesDown: {NtupleFilesDown}
  WeightDown: weight_sys_radLo*(1.0/weight_nominal)
  WeightUp: weight_sys_radHi*(1.0/weight_nominal)
  Category: "Modelling"'''
    block = _ttbar_addrad.format(
        Smoothing=Smoothing,
        NtupleFilesUp=NtupleFilesUp,
        NtupleFilesDown=NtupleFilesDown,
    )
    return block


def Systematic_tW_DRDS(NtupleFilesUp="", Smoothing=""):
    _tw_drds = '''
Systematic: "tW_DRDS"
  Title: "tW DR vs. DS"
  Type: HISTO
  Samples: tW
  NtupleFilesUp: {NtupleFilesUp}
  Smoothing: {Smoothing}
  Symmetrisation: ONESIDED
  Category: "Modelling"'''
    block = _tw_drds.format(Smoothing=Smoothing, NtupleFilesUp=NtupleFilesUp)
    return block


def Systematic_HISTO_ReqRefSamp(Name="", Title="",
                                Sample="", ReferenceSample="",
                                NtupleFilesUp="", Smoothing=""):
    _altsample_systematic = '''
Systematic: "{Name}"
  Title: "{Title}"
  Type: HISTO
  Samples: {Sample}
  ReferenceSample: {ReferenceSample}
  NtupleFilesUp: {NtupleFilesUp}
  Smoothing: {Smoothing}
  Symmetrisation: ONESIDED
  Category: "Modelling"'''
    block = _altsample_systematic.format(
        Name=Name,
        Title=Title,
        Sample=Sample,
        ReferenceSample=ReferenceSample,
        NtupleFilesUp=NtupleFilesUp,
        Smoothing=Smoothing,
    )
    return block
