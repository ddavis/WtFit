Job: "tW_v25_cut_mbl_4j"
  CmeLabel: "13 TeV"
  POI: "SigXsecOverSM"
  ReadFrom: NTUP
  NtuplePaths: "/home/drd25/ATLAS/project_space/top/analysis/run/with_mbl_split/bdt/nominal"
  Label: "#it{e^{#pm}#mu^{#mp}}"
  LumiLabel: "80.5 fb^{-1}"
  MCweight: "weight_nominal"
  Lumi: 80.5
  PlotOptions: YIELDS,NOXERR
  NtupleName: "WtTMVA_nominal"
  DebugLevel: 0
  MCstatThreshold: 0.001
  %MCstatConstraint: "Poisson"
  SystControlPlots: TRUE
  SystPruningShape: 0.005
  SystPruningNorm: 0.005
  %CorrelationThreshold: 0.20
  %HistoChecks: NOCRASH
  ImageFormat: "pdf"
  SystCategoryTables: TRUE
  RankingPlot: all
  RankingMaxNP: 15
  DoSummaryPlot: TRUE
  DoTables: TRUE
  DoSignalRegionsPlot: TRUE
  DoPieChartPlot: TRUE


Fit: fit
  FitType: SPLUSB
  FitRegion: CRSR
  NumCPU: 8
  POIAsimov: 1
  FitBlind: TRUE
  UseMinos: SigXsecOverSM


Region: "reg1j1b"
  Type: SIGNAL
  Selection: "reg1j1b==1&&elmu==1&&OS==1"
  VariableTitle: "BDT"
  Variable: "bdt_response",20,-0.8,0.8
  Label: "1j1b"
  ShortLabel: "1j1b"
  Binning: "AutoBin","TransfoD",6.,6.

Region: "reg2j1b"
  Type: SIGNAL
  Selection: "reg2j1b==1&&elmu==1&&OS==1"
  Variable: "bdt_response",20,-0.8,0.8
  VariableTitle: "BDT"
  Label: "2j1b"
  ShortLabel: "2j1b"
  Binning: "AutoBin","TransfoD",6.,6.

Region: "reg2j2b"
  Type: SIGNAL
  Selection: "reg2j2b==1&&elmu==1&&OS==1&&minimaxmbl<=150.0"
  Variable: "bdt_response",20,-0.4,0.6
  VariableTitle: "BDT"
  Label: "2j2b"
  ShortLabel: "2j2b"
  Binning: "AutoBin","TransfoD",6.,6.

Region: "reg4j"
  Type: CONTROL
  Selection: "njets>=4&&elmu==1&&OS==1"
  Variable: "pT_jet2",20,25,225
  VariableTitle: "#it{p}_{T}^{jet2}"
  Label: "4j"
  ShortLabel: "4j"

Sample: tWghost
  Title: "tWghost"
  Type: GHOST
  NtupleFiles: tW_DR_410648_AFII_MC16a_nominal.bdt_response,tW_DR_410649_AFII_MC16a_nominal.bdt_response,tW_DR_410648_AFII_MC16d_nominal.bdt_response,tW_DR_410649_AFII_MC16d_nominal.bdt_response


Sample: ttbarghost
  Title: "ttbarghost"
  Type: GHOST
  NtupleFiles: ttbar_410472_AFII_MC16a_nominal.bdt_response,ttbar_410472_AFII_MC16d_nominal.bdt_response


Sample: Data
  Type: DATA
  Title: "Data"
  NtupleFiles: Data15_data15_Data_Data_nominal.bdt_response,Data16_data16_Data_Data_nominal.bdt_response,Data17_data17_Data_Data_nominal.bdt_response


Sample: "tW"
  Type: SIGNAL
  Title: "#it{tW}"
  TexTitle: "$tW$"
  FillColor: 861
  LineColor: 1
  NtupleFiles: tW_DR_410648_FS_MC16a.bdt_response,tW_DR_410649_FS_MC16a.bdt_response,tW_DR_410648_FS_MC16d.bdt_response,tW_DR_410649_FS_MC16d.bdt_response


Sample: "ttbar"
  Type: BACKGROUND
  Title: "#it{t#bar{t}}"
  TexTitle: "$t\bar{t}$"
  FillColor: 633
  LineColor: 1
  NtupleFiles: ttbar_410472_FS_MC16a.bdt_response,ttbar_410472_FS_MC16d.bdt_response


Sample: "Diboson"
  Type: BACKGROUND
  Title: "Diboson"
  TexTitle: "Diboson"
  FillColor: 400
  LineColor: 1
  NtupleFiles: Diboson_all

Sample: "Zjets"
  Type: BACKGROUND
  Title: "#it{Z}+jets"
  TexTitle: "$Z+\mathrm{jets}$"
  FillColor: 801
  LineColor: 1
  NtupleFiles: Zjets_all

Sample: "MCNP"
  Type: BACKGROUND
  Title: "MCNP"
  TexTitle: "MCNP"
  FillColor: 882
  NtupleFiles: MCNP_all

NormFactor: SigXsecOverSM
  Title: "#it{#mu}_{#it{tW}}"
  Nominal: 1
  Min: -100
  Max: 100
  Samples: tW


NormFactor: "mu_tt"
  Title: "#it{#mu}_{#it{t#bar{t}}}"
  Nominal: 1
  Min: 0
  Max: 2
  Samples: ttbar


NormFactor: "muLumi"
  Title: "muLumi"
  Nominal: 1
  Min: 0
  Max: 100
  Samples: all
  Constant: TRUE

Systematic: "Lumi"
  Title: "Luminosity"
  Type: OVERALL
  OverallUp: 0.023
  OverallDown: -0.023
  Samples: all
  Category: Instrumental


Systematic: "Norm_Z"
  Title: "Norm Z"
  Type: OVERALL
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: Zjets
  Category: Norms


Systematic: Norm_Diboson
  Title: "Norm Diboson"
  Type: OVERALL
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: Diboson
  Category: Norms


Systematic: jvt;pileup;leptonSF_EL_SF_Trigger;leptonSF_EL_SF_Reco;leptonSF_EL_SF_ID;leptonSF_EL_SF_Isol;leptonSF_MU_SF_Trigger_STAT;leptonSF_MU_SF_Trigger_SYST;leptonSF_MU_SF_ID_STAT;leptonSF_MU_SF_ID_SYST;leptonSF_MU_SF_ID_STAT_LOWPT;leptonSF_MU_SF_ID_SYST_LOWPT;leptonSF_MU_SF_Isol_STAT;leptonSF_MU_SF_Isol_SYST;leptonSF_MU_SF_TTVA_STAT;leptonSF_MU_SF_TTVA_SYST
  Title: jvt;pileup;leptonSF_EL_SF_Trigger;leptonSF_EL_SF_Reco;leptonSF_EL_SF_ID;leptonSF_EL_SF_Isol;leptonSF_MU_SF_Trigger_STAT;leptonSF_MU_SF_Trigger_SYST;leptonSF_MU_SF_ID_STAT;leptonSF_MU_SF_ID_SYST;leptonSF_MU_SF_ID_STAT_LOWPT;leptonSF_MU_SF_ID_SYST_LOWPT;leptonSF_MU_SF_Isol_STAT;leptonSF_MU_SF_Isol_SYST;leptonSF_MU_SF_TTVA_STAT;leptonSF_MU_SF_TTVA_SYST
  Type: HISTO
  Samples: tW, ttbar
  WeightUp: (1.0/weight_nominal)*weight_sys_jvt_UP;(1.0/weight_nominal)*weight_sys_pileup_UP;(1.0/weight_nominal)*weight_sys_leptonSF_EL_SF_Trigger_UP;(1.0/weight_nominal)*weight_sys_leptonSF_EL_SF_Reco_UP;(1.0/weight_nominal)*weight_sys_leptonSF_EL_SF_ID_UP;(1.0/weight_nominal)*weight_sys_leptonSF_EL_SF_Isol_UP;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_Trigger_STAT_UP;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_Trigger_SYST_UP;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_ID_STAT_UP;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_ID_SYST_UP;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_UP;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_UP;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_Isol_STAT_UP;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_Isol_SYST_UP;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_TTVA_STAT_UP;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_TTVA_SYST_UP
  WeightDown: (1.0/weight_nominal)*weight_sys_jvt_DOWN;(1.0/weight_nominal)*weight_sys_pileup_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_EL_SF_Trigger_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_EL_SF_Reco_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_EL_SF_ID_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_EL_SF_Isol_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_Trigger_STAT_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_Trigger_SYST_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_ID_STAT_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_ID_SYST_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_Isol_STAT_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_Isol_SYST_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_TTVA_STAT_DOWN;(1.0/weight_nominal)*weight_sys_leptonSF_MU_SF_TTVA_SYST_DOWN
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: Instrumental

Systematic: bTagSF_MV2c10_77_eigenvars_B_0;bTagSF_MV2c10_77_eigenvars_B_1;bTagSF_MV2c10_77_eigenvars_B_2;bTagSF_MV2c10_77_eigenvars_B_3;bTagSF_MV2c10_77_eigenvars_B_4;bTagSF_MV2c10_77_eigenvars_B_5;bTagSF_MV2c10_77_eigenvars_B_6;bTagSF_MV2c10_77_eigenvars_B_7;bTagSF_MV2c10_77_eigenvars_B_8;bTagSF_MV2c10_77_eigenvars_C_0;bTagSF_MV2c10_77_eigenvars_C_1;bTagSF_MV2c10_77_eigenvars_C_2;bTagSF_MV2c10_77_eigenvars_Light_0;bTagSF_MV2c10_77_eigenvars_Light_1;bTagSF_MV2c10_77_eigenvars_Light_2;bTagSF_MV2c10_77_eigenvars_Light_3;bTagSF_MV2c10_77_eigenvars_Light_10;bTagSF_MV2c10_77_extrapolation;bTagSF_MV2c10_77_extrapolation_from_charm
  Title: bTagSF_MV2c10_77_ev_B_0;bTagSF_MV2c10_77_ev_B_1;bTagSF_MV2c10_77_ev_B_2;bTagSF_MV2c10_77_ev_B_3;bTagSF_MV2c10_77_ev_B_4;bTagSF_MV2c10_77_ev_B_5;bTagSF_MV2c10_77_ev_B_6;bTagSF_MV2c10_77_ev_B_7;bTagSF_MV2c10_77_ev_B_8;bTagSF_MV2c10_77_ev_C_0;bTagSF_MV2c10_77_ev_C_1;bTagSF_MV2c10_77_ev_C_2;bTagSF_MV2c10_77_ev_Light_0;bTagSF_MV2c10_77_ev_Light_1;bTagSF_MV2c10_77_ev_Light_2;bTagSF_MV2c10_77_ev_Light_3;bTagSF_MV2c10_77_ev_Light_10;bTagSF_MV2c10_77_extrapolation;bTagSF_MV2c10_77_extrapolation_from_charm
  Type: HISTO
  Samples: tW, ttbar
  WeightDown: (1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_0_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_1_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_2_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_3_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_4_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_5_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_6_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_7_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_8_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_C_0_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_C_1_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_C_2_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_Light_0_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_Light_1_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_Light_2_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_Light_3_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_extrapolation_down;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_extrapolation_from_charm_down
  WeightUp: (1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_0_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_1_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_2_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_3_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_4_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_5_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_6_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_7_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_B_8_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_C_0_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_C_1_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_C_2_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_Light_0_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_Light_1_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_Light_2_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_eigenvars_Light_3_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_extrapolation_up;(1.0/weight_nominal)*weight_sys_bTagSF_MV2c10_77_extrapolation_from_charm_up
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: FlavorTagging


Systematic: "EG_Resolution"
  Title: "Electron energy resolution"
  Type: HISTO
  Samples: tW,ttbar
  NtupleNameUp: WtTMVA_EG_RESOLUTION_ALL__1up
  NtupleNameDown: WtTMVA_EG_RESOLUTION_ALL__1down
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalEG

Systematic: "EG_Scale"
  Title: "Electron energy scale"
  Type: HISTO
  Samples: tW,ttbar
  NtupleNameUp: WtTMVA_EG_SCALE_ALL__1up
  NtupleNameDown: WtTMVA_EG_SCALE_ALL__1down
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalEG

Systematic: "DRDS"
  Title: "tW DRDS"
  Type: HISTO
  Samples: tW
  NtupleFilesUp: tW_DS_410656_FS_MC16a_nominal.bdt_response,tW_DS_410657_FS_MC16a_nominal.bdt_response,tW_DS_410656_FS_MC16d_nominal.bdt_response,tW_DS_410657_FS_MC16d_nominal.bdt_response
  Smoothing: 40
  Symmetrisation: ONESIDED
  Category: Modelling


Systematic: "tW_PS"
  Title: "tW Parton Shower"
  Type: HISTO
  Samples: tW
  ReferenceSample: tWghost
  Smoothing: 40
  Symmetrisation: ONESIDED
  NtupleFilesUp: tW_DR_411038_AFII_MC16a_nominal.bdt_response,tW_DR_411038_AFII_MC16d_nominal.bdt_response,tW_DR_411039_AFII_MC16a_nominal.bdt_response,tW_DR_411039_AFII_MC16d_nominal.bdt_response
  Category: Modelling


Systematic: "ttbar_PS"
  Title: "t#bar{t} Parton Shower"
  Type: HISTO
  Samples: ttbar
  ReferenceSample: ttbarghost
  Smoothing: 40
  Symmetrisation: ONESIDED
  NtupleFilesUp: ttbar_410558_AFII_MC16a_nominal.bdt_response,ttbar_410558_AFII_MC16d_nominal.bdt_response
  Category: Modelling


Systematic: "tW_HS"
  Title: "tW Hard Scatter"
  Type: HISTO
  Samples: tW
  Smoothing: 40
  ReferenceSample: tWghost
  Symmetrisation: ONESIDED
  NtupleFilesUp: tW_412003_AFII_MC16a_nominal.bdt_response,tW_412003_AFII_MC16d_nominal.bdt_response
  Category: Modelling


Systematic: "ttbar_HS"
  Title: "t#bar{t} Hard Scatter"
  Type: HISTO
  Samples: ttbar
  Smoothing: 40
  ReferenceSample: ttbarghost
  Symmetrisation: ONESIDED
  NtupleFilesUp: ttbar_410465_AFII_MC16a_nominal.bdt_response,ttbar_410465_AFII_MC16d_nominal.bdt_response
  Category: Modelling


Systematic: "Jet_BJES_Response"
  Title: "Jet_BJES_Response"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_BJES_Response__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_BJES_Response__1up

Systematic: "Jet_EffNP_Detector1"
  Title: "Jet_EffNP_Detector1"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Detector1__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Detector1__1up

Systematic: "Jet_EffNP_Mixed1"
  Title: "Jet_EffNP_Mixed1"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed1__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed1__1up

Systematic: "Jet_EffNP_Mixed2"
  Title: "Jet_EffNP_Mixed2"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed2__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed2__1up

Systematic: "Jet_EffNP_Mixed3"
  Title: "Jet_EffNP_Mixed3"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed3__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed3__1up

Systematic: "Jet_EffNP_Modelling1"
  Title: "Jet_EffNP_Modelling1"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling1__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling1__1up

Systematic: "Jet_EffNP_Modelling2"
  Title: "Jet_EffNP_Modelling2"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling2__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling2__1up

Systematic: "Jet_EffNP_Modelling3"
  Title: "Jet_EffNP_Modelling3"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling3__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling3__1up

Systematic: "Jet_EffNP_Modelling4"
  Title: "Jet_EffNP_Modelling4"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling4__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling4__1up

Systematic: "Jet_EffNP_Statistical1"
  Title: "Jet_EffNP_Statistical1"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical1__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical1__1up

Systematic: "Jet_EffNP_Statistical2"
  Title: "Jet_EffNP_Statistical2"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical2__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical2__1up

Systematic: "Jet_EffNP_Statistical3"
  Title: "Jet_EffNP_Statistical3"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical3__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical3__1up

Systematic: "Jet_EffNP_Statistical4"
  Title: "Jet_EffNP_Statistical4"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical4__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical4__1up

Systematic: "Jet_EffNP_Statistical5"
  Title: "Jet_EffNP_Statistical5"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical5__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical5__1up

Systematic: "Jet_EffNP_Statistical6"
  Title: "Jet_EffNP_Statistical6"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical6__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical6__1up

Systematic: "Jet_EtaIntCalib_Modelling"
  Title: "Jet_EtaIntCalib_Modelling"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1up

Systematic: "Jet_EtaIntCalib_NonClosure_highE"
  Title: "Jet_EtaIntCalib_NonClosure_highE"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up

Systematic: "Jet_EtaIntCalib_NonClosure_negEta"
  Title: "Jet_EtaIntCalib_NonClosure_negEta"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up

Systematic: "Jet_EtaIntCalib_NonClosure_posEta"
  Title: "Jet_EtaIntCalib_NonClosure_posEta"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up

Systematic: "Jet_EtaIntCalib_TotalStat"
  Title: "Jet_EtaIntCalib_TotalStat"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1up

Systematic: "Jet_Flavor_Composition"
  Title: "Jet_Flavor_Composition"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_Flavor_Composition__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_Flavor_Composition__1up

Systematic: "Jet_Flavor_Response"
  Title: "Jet_Flavor_Response"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_Flavor_Response__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_Flavor_Response__1up

Systematic: "Jet_JER_DataVsMC"
  Title: "Jet_JER_DataVsMC"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_JER_DataVsMC__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_JER_DataVsMC__1up

Systematic: "Jet_JER_EffNP_1"
  Title: "Jet_JER_EffNP_1"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_1__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_1__1up

Systematic: "Jet_JER_EffNP_2"
  Title: "Jet_JER_EffNP_2"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_2__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_2__1up

Systematic: "Jet_JER_EffNP_3"
  Title: "Jet_JER_EffNP_3"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_3__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_3__1up

Systematic: "Jet_JER_EffNP_4"
  Title: "Jet_JER_EffNP_4"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_4__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_4__1up

Systematic: "Jet_JER_EffNP_5"
  Title: "Jet_JER_EffNP_5"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_5__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_5__1up

Systematic: "Jet_JER_EffNP_6"
  Title: "Jet_JER_EffNP_6"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_6__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_6__1up

Systematic: "Jet_JER_EffNP_7restTerm"
  Title: "Jet_JER_EffNP_7restTerm"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_7restTerm__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up


Systematic: "Jet_Pileup_OffsetMu"
  Title: "Jet_Pileup_OffsetMu"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_Pileup_OffsetMu__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_Pileup_OffsetMu__1up

Systematic: "Jet_Pileup_OffsetNPV"
  Title: "Jet_Pileup_OffsetNPV"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_Pileup_OffsetNPV__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_Pileup_OffsetNPV__1up

Systematic: "Jet_Pileup_PtTerm"
  Title: "Jet_Pileup_PtTerm"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_Pileup_PtTerm__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_Pileup_PtTerm__1up

Systematic: "Jet_Pileup_RhoTopology"
  Title: "Jet_Pileup_RhoTopology"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_Pileup_RhoTopology__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_Pileup_RhoTopology__1up

Systematic: "Jet_PunchThrough_MC16"
  Title: "Jet_PunchThrough_MC16"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_PunchThrough_MC16__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_PunchThrough_MC16__1up

Systematic: "Jet_SingleParticle_HighPt"
  Title: "Jet_SingleParticle_HighPt"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalJet
  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_SingleParticle_HighPt__1down
  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_SingleParticle_HighPt__1up

Systematic: "Muon_ID"
  Title: "Muon ID"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalMuon
  NtupleNameDown: WtTMVA_MUON_ID__1down
  NtupleNameUp: WtTMVA_MUON_ID__1up

Systematic: "Muon_SAGITTA_RHO"
  Title: "Muon SAGITTA_RHO"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalMuon
  NtupleNameDown: WtTMVA_MUON_SAGITTA_RHO__1down
  NtupleNameUp: WtTMVA_MUON_SAGITTA_RHO__1up

Systematic: "Muon_MS"
  Title: "Muon MS"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalMuon
  NtupleNameDown: WtTMVA_MUON_MS__1down
  NtupleNameUp: WtTMVA_MUON_MS__1up

Systematic: "Muon_SCALE"
  Title: "Muon SCALE"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalMuon
  NtupleNameDown: WtTMVA_MUON_SCALE__1down
  NtupleNameUp: WtTMVA_MUON_SCALE__1up

Systematic: "Muon_SAGITTA_RESBIAS"
  Title: "Muon SAGITTA_RESBIAS"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalMuon
  NtupleNameDown: WtTMVA_MUON_SAGITTA_RESBIAS__1down
  NtupleNameUp: WtTMVA_MUON_SAGITTA_RESBIAS__1up

Systematic: "MET_SoftTrk_ResoPara"
  Title: "MET SoftTrk ResoPara"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing:40
  Symmetrisation: ONESIDED
  Category: InstrumentalMET
  NtupleNameUp: WtTMVA_MET_SoftTrk_ResoPara

Systematic: "MET_SoftTrk_ResoPerp"
  Title: "MET SoftTrk ResoPerp"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing:40
  Symmetrisation: ONESIDED
  Category: InstrumentalMET
  NtupleNameUp: WtTMVA_MET_SoftTrk_ResoPerp

Systematic: "MET_SoftTrk_Scale"
  Title: "MET SoftTrk Scale"
  Type: HISTO
  Samples: tW,ttbar
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: InstrumentalMET
  NtupleNameDown: WtTMVA_MET_SoftTrk_ScaleDown
  NtupleNameUp: WtTMVA_MET_SoftTrk_ScaleUp
