Job: "tW_simpleBDT_77"
  CmeLabel: "13 TeV"
  POI: "SigXsecOverSM"
  ReadFrom: "NTUP"
  NtuplePaths: "/var/phy/project/hep/atlas/users/drd25/top/analysis/run/v25/bdt_out"
  Label: "#it{e^{#pm}#mu^{#mp}}"
  LumiLabel: "80 fb^{-1}"
  MCweight: "weight_nominal"
  Lumi: 80.0
  PlotOptions: YIELDS,NOXERR
  NtupleName: "WtTMVA_nominal"
  DebugLevel: 0
  MCstatThreshold: 0.001
  %MCstatConstraint: "Poisson"
  SystControlPlots: TRUE
  SystPruningShape: 0.005
  SystPruningNorm: 0.005
  %CorrelationThreshold: 0.20
  %HistoChecks: NOCRASH
  ImageFormat: "pdf"
  SystCategoryTables: TRUE
  RankingPlot: all
  RankingMaxNP: 15
  DoSummaryPlot: TRUE
  DoTables: TRUE
  DoSignalRegionsPlot: TRUE
  DoPieChartPlot: TRUE

Fit: fit
  FitType: SPLUSB
  FitRegion: CRSR
  NumCPU: 8
  POIAsimov: 1
  FitBlind: TRUE
  UseMinos: SigXsecOverSM

Region: reg1j1b
  Type: SIGNAL
  Selection: reg1j1b==1&&elmu==1&&OS==1
  VariableTitle: BDT
  Variable: bdt_response,20,-0.5,0.4
  Label: 1j1b
  ShortLabel: 1j1b
  Binning: "AutoBin","TransfoD",8.,15.

Region: reg2j1b
  Type: SIGNAL
  Selection: reg2j1b==1&&elmu==1&&OS==1
  Variable: bdt_response,20,-0.15,0.2
  VariableTitle: BDT
  Label: 2j1b
  ShortLabel: 2j1b
  Binning: "AutoBin","TransfoD",8.,15.

Region: reg2j2b
  Type: SIGNAL
  Selection: reg2j2b==1&&elmu==1&&OS==1
  Variable: bdt_response,20,-0.15,0.15
  VariableTitle: BDT
  Label: 2j2b
  ShortLabel: 2j2b
  Binning: "AutoBin","TransfoD",8.,15.

Sample: tWghost
  Type: GHOST
  NtupleFiles: tW_DR_410648_AFII_MC16a.bdt_response,tW_DR_410649_AFII_MC16a.bdt_response,tW_DR_410648_AFII_MC16d.bdt_response,tW_DR_410649_AFII_MC16d.bdt_response

Sample: ttbarghost
  Type: GHOST
  NtupleFiles: ttbar_410472_AFII_MC16a.bdt_response,ttbar_410472_AFII_MC16d.bdt_response

Sample: Data
  Type: DATA
  Title: Data
  NtupleFiles: Data_data15.bdt_response,Data_data16.bdt_response,Data_data17.bdt_response

Sample: tW
  Type: SIGNAL
  Title: #it{tW}
  TexTitle: $tW$
  FillColor: 861
  LineColor: 1
  NtupleFiles: tW_DR_410648_FS_MC16a.bdt_response,tW_DR_410649_FS_MC16a.bdt_response,tW_DR_410648_FS_MC16d.bdt_response,tW_DR_410649_FS_MC16d.bdt_response

Sample: ttbar
  Type: BACKGROUND
  Title: #it{t#bar{t}}
  TexTitle: $t\bar{t}$
  FillColor: 633
  LineColor: 1
  NtupleFiles: ttbar_410472_FS_MC16a.bdt_response,ttbar_410472_FS_MC16d.bdt_response

Sample: Diboson
  Type: BACKGROUND
  Title: Diboson
  TexTitle: Diboson
  FillColor: 400
  LineColor: 1
  NtupleFiles: Diboson_364250_FS_MC16a.bdt_response,Diboson_364253_FS_MC16a.bdt_response,Diboson_364254_FS_MC16a.bdt_response,Diboson_364250_FS_MC16d.bdt_response,Diboson_364253_FS_MC16d.bdt_response,Diboson_364254_FS_MC16d.bdt_response

Sample: Ztautau
  Type: BACKGROUND
  Title: #it{Z}#rightarrow#tau#tau
  TexTitle: $Z\rightarrow\tau\tau$
  FillColor: 801
  LineColor: 1
  NtupleFiles: Ztautau_364128_FS_MC16a.bdt_response,Ztautau_364129_FS_MC16a.bdt_response,Ztautau_364130_FS_MC16a.bdt_response,Ztautau_364131_FS_MC16a.bdt_response,Ztautau_364132_FS_MC16a.bdt_response,Ztautau_364133_FS_MC16a.bdt_response,Ztautau_364134_FS_MC16a.bdt_response,Ztautau_364135_FS_MC16a.bdt_response,Ztautau_364136_FS_MC16a.bdt_response,Ztautau_364137_FS_MC16a.bdt_response,Ztautau_364138_FS_MC16a.bdt_response,Ztautau_364139_FS_MC16a.bdt_response,Ztautau_364140_FS_MC16a.bdt_response,Ztautau_364141_FS_MC16a.bdt_response,Ztautau_364128_FS_MC16d.bdt_response,Ztautau_364129_FS_MC16d.bdt_response,Ztautau_364130_FS_MC16d.bdt_response,Ztautau_364131_FS_MC16d.bdt_response,Ztautau_364132_FS_MC16d.bdt_response,Ztautau_364133_FS_MC16d.bdt_response,Ztautau_364134_FS_MC16d.bdt_response,Ztautau_364135_FS_MC16d.bdt_response,Ztautau_364136_FS_MC16d.bdt_response,Ztautau_364137_FS_MC16d.bdt_response,Ztautau_364138_FS_MC16d.bdt_response,Ztautau_364139_FS_MC16d.bdt_response,Ztautau_364140_FS_MC16d.bdt_response,Ztautau_364141_FS_MC16d.bdt_response

NormFactor: SigXsecOverSM
  Title: #it{#mu}_{#it{tW}}
  Nominal: 1
  Min: -100
  Max: 100
  Samples: tW

NormFactor: mu_tt
  Title: #it{#mu}_{#it{t#bar{t}}}
  Nominal: 1
  Min: 0
  Max: 2
  Samples: ttbar

NormFactor: muLumi
  Nominal: 1
  Min: 0
  Max: 100
  Samples: all
  Constant: TRUE

Systematic: Lumi
  Title: Luminosity
  Type: OVERALL
  OverallUp: 0.032
  OverallDown: -0.032
  Samples: all
  Category: Instrumental

Systematic: Norm_Z
  Title: "Norm Z"
  Type: OVERALL
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: Ztautau
  Category: Norms

Systematic: Norm_Diboson
  Title: "Norm Diboson"
  Type: OVERALL
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: Diboson
  Category: Norms

Systematic: EG_Resolution;EG_Scale
  Title: Electron energy resolution;Electron energy scale
  Type: HISTO
  Samples: tW,ttbar
  NtupleNameUp: WtTMVA_EG_RESOLUTION_ALL__1up;WtTMVA_EG_SCALE_ALL__1up
  NtupleNameDown: WtTMVA_EG_RESOLUTION_ALL__1down;WtTMVA_EG_SCALE_ALL__1down
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: Instrumental

Systematic: Muons_ID;Muons_MS;Muons_Scale;Muons_Sagitta_ResBias;Muons_Sagitta_Rho
  Title: Muon momentum resolution (ID) ;Muon momentum resolution (MS);Muon momentum scale;Muon Sagitta Res Bias;Muons Sagitta Rho
  Type: HISTO
  Samples: ttbar, tW
  NtupleNameUp: WtTMVA_MUON_ID__1up;WtTMVA_MUON_MS__1up;WtTMVA_MUON_SCALE__1up;WtTMVA_MUON_SAGITTA_RESBIAS__1up;WtTMVA_MUON_SAGITTA_RHO__1up
  NtupleNameDown: WtTMVA_MUON_ID__1down;WtTMVA_MUON_MS__1down;WtTMVA_MUON_SCALE__1down;WtTMVA_MUON_SAGITTA_RESBIAS__1down;WtTMVA_MUON_SAGITTA_RHO__1down
  Smoothing: 40
  Symmetrisation: TWOSIDED
  Category: Instrumental

%Systematic: JER
%  Title: JER
%  Type: HISTO
%  Samples: tW,ttbar
%  NtupleNameUp: WtTMVA_JET_JER_SINGLE_NP__1up
%  Smoothing: 40
%  Symmetrisation: ONESIDED
%  Category: Instrumental

Systematic: DRDS
  Title: "tW DRDS"
  Type: HISTO
  Samples: tW
  NtupleFilesUp: tW_DS_410656_FS_MC16a.bdt_response,tW_DS_410657_FS_MC16a.bdt_response,tW_DS_410656_FS_MC16d.bdt_response,tW_DS_410657_FS_MC16d.bdt_response
  Smoothing: 40
  Symmetrisation: ONESIDED
  Category: Modelling

%Systematic: JET_BJES_Response;JET_EffectiveNP_Detector1;JET_EffectiveNP_Mixed1;JET_EffectiveNP_Mixed2;JET_EffectiveNP_Mixed3;JET_EffectiveNP_Modelling1;JET_EffectiveNP_Modelling2;JET_EffectiveNP_Modelling3;JET_EffectiveNP_Modelling4;JET_EffectiveNP_Statistical1;JET_EffectiveNP_Statistical2;JET_EffectiveNP_Statistical3;JET_EffectiveNP_Statistical4;JET_EffectiveNP_Statistical5;JET_EffectiveNP_Statistical6;JET_EtaIntercalibration_Modelling;JET_EtaIntercalibration_NonClosure_highE;JET_EtaIntercalibration_NonClosure_negEta;JET_EtaIntercalibration_NonClosure_posEta;JET_EtaIntercalibration_TotalStat;JET_Flavor_Composition;JET_Flavor_Response;JET_Pileup_OffsetMu;JET_Pileup_OffsetNPV;JET_Pileup_PtTerm;JET_Pileup_RhoTopology;JET_PunchThrough_MC16;JET_SingleParticle_HighPt
%  Title: JET_BJES_Response;JET_EffNP_Detector1;JET_EffNP_Mixed1;JET_EffNP_Mixed2;JET_EffNP_Mixed3;JET_EffNP_Modelling1;JET_EffNP_Modelling2;JET_EffNP_Modelling3;JET_EffNP_Modelling4;JET_EffNP_Statistical1;JET_EffNP_Statistical2;JET_EffNP_Statistical3;JET_EffNP_Statistical4;JET_EffNP_Statistical5;JET_EffNP_Statistical6;JET_EtaIntercalibration_Modelling;JET_EtaIntercalibration_NonClosure_highE;JET_EtaIntercalibration_NonClosure_negEta;JET_EtaIntercalibration_NonClosure_posEta;JET_EtaIntercalibration_TotalStat;JET_Flavor_Composition;JET_Flavor_Response;JET_Pileup_OffsetMu;JET_Pileup_OffsetNPV;JET_Pileup_PtTerm;JET_Pileup_RhoTopology;JET_PunchThrough_MC16;JET_SingleParticle_HighPt
%  Type: HISTO
%  Samples: tW, ttbar
%  NtupleNameUp: WtTMVA_JET_CategoryReduction_JET_BJES_Response__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Detector1__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed1__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed2__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed3__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling1__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling2__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling3__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling4__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical1__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical2__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical3__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical4__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical5__1up;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical6__1up;WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1up;WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up;WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up;WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up;WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1up;WtTMVA_JET_CategoryReduction_JET_Flavor_Composition__1up;WtTMVA_JET_CategoryReduction_JET_Flavor_Response__1up;WtTMVA_JET_CategoryReduction_JET_Pileup_OffsetMu__1up;WtTMVA_JET_CategoryReduction_JET_Pileup_OffsetNPV__1up;WtTMVA_JET_CategoryReduction_JET_Pileup_PtTerm__1up;WtTMVA_JET_CategoryReduction_JET_Pileup_RhoTopology__1up;WtTMVA_JET_CategoryReduction_JET_PunchThrough_MC16__1up;WtTMVA_JET_CategoryReduction_JET_SingleParticle_HighPt__1up
%  NtupleNameDown: WtTMVA_JET_CategoryReduction_JET_BJES_Response__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Detector1__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed1__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed2__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Mixed3__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling1__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling2__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling3__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Modelling4__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical1__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical2__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical3__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical4__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical5__1down;WtTMVA_JET_CategoryReduction_JET_EffectiveNP_Statistical6__1down;WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1down;WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down;WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down;WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down;WtTMVA_JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1down;WtTMVA_JET_CategoryReduction_JET_Flavor_Composition__1down;WtTMVA_JET_CategoryReduction_JET_Flavor_Response__1down;WtTMVA_JET_CategoryReduction_JET_Pileup_OffsetMu__1down;WtTMVA_JET_CategoryReduction_JET_Pileup_OffsetNPV__1down;WtTMVA_JET_CategoryReduction_JET_Pileup_PtTerm__1down;WtTMVA_JET_CategoryReduction_JET_Pileup_RhoTopology__1down;WtTMVA_JET_CategoryReduction_JET_PunchThrough_MC16__1down;WtTMVA_JET_CategoryReduction_JET_SingleParticle_HighPt__1down
%  Smoothing: 40
%  Symmetrisation: TWOSIDED
%  Category: Instrumental


Systematic: tW_PS
  Title: "tW Parton Shower"
  Type: HISTO
  Samples: tW
  ReferenceSample: tWghost
  Smoothing: 40
  Symmetrisation: ONESIDED
  NtupleFilesUp: tW_syst_411038_AFII_MC16a.bdt_response,tW_syst_411038_AFII_MC16d.bdt_response,tW_syst_411039_AFII_MC16a.bdt_response,tW_syst_411039_AFII_MC16d.bdt_response
  Category: Modelling

Systematic: ttbar_PS
  Title: "t#bar{t} Parton Shower"
  Type: HISTO
  Samples: ttbar
  ReferenceSample: ttbarghost
  Smoothing: 40
  Symmetrisation: ONESIDED
  NtupleFilesUp: ttbar_syst_410558_AFII_MC16a.bdt_response,ttbar_syst_410558_AFII_MC16d.bdt_response
  Category: Modelling


Systematic: tW_HS
  Title: "tW Hard Scatter"
  Type: HISTO
  Samples: tW
  Smoothing: 40
  ReferenceSample: tWghost
  Symmetrisation: ONESIDED
  NtupleFilesUp: tW_syst_412003_AFII_MC16a.bdt_response,tW_syst_412003_AFII_MC16d.bdt_response
  Category: Modelling

Systematic: ttbar_HS
  Title: "t#bar{t} Hard Scatter"
  Type: HISTO
  Samples: ttbar
  Smoothing: 40
  ReferenceSample: ttbarghost
  Symmetrisation: ONESIDED
  NtupleFilesUp: ttbar_syst_410465_AFII_MC16a.bdt_response,ttbar_syst_410465_AFII_MC16d.bdt_response
  Category: Modelling
