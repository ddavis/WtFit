# For emacs pyls

from setuptools import setup
from setuptools import find_packages
import os

LD = """Wrapper around TRExFitter for constructing fitting
configurations for the tW Analysis"""


with open("requirements.txt") as f:
    requirements = f.read().splitlines()


setup(
    name="WtFit",
    version="0.0.1",
    scripts=[],
    packages=find_packages(exclude=["tests"]),
    description="Fitting for the tW analysis",
    long_description=LD,
    author="Doug Davis",
    author_email="ddavis@cern.ch",
    license="MIT",
    url="https://gitlab.cern.ch/ddavis/WtFit",
    install_requires=requirements,
    classifiers=[
        "Intended Audience :: Science/Research",
        "Programming Language :: Python :: 3.6",
    ],
)
